GamePath = new Mongo.Collection('game_path');
/*
 * Add query methods like this:
 *  GamePath.findPublic = function () {
 *    return GamePath.find({is_public: true});
 *  }
 */
 GamePath.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  // source np request
 sourceNPRequestCode:{
    type:String
  },
  // source player response
  sourcePlayerResponseCode:{
    type:String
  },
  // destination scenario code
  destinationScenarioCode:{
    type:String
  },
  // destination nonplayable character code
  destinationNPCharacterCode:{
    type:String
  },
  // npcharacter request code
 destinationNPCharacterRequestCode:{
    type:String
  }

  
}));